﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace AutoCinemaEstelar.ViewModels
{
    class MainViewModel
    {
        public string url { get; set; }
        public MainViewModel()
        {
            this.url = "https://www.autocinemaestelar.com/";
        }
    }
}
